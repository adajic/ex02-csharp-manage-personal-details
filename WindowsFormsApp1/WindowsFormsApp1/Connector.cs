﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Connector
    {
        SqlConnection conn;

        public Connector(String connectionString)
        {
            conn = new SqlConnection();
            conn.ConnectionString = connectionString;
        }

        public void changeSqlConnectionStringBuilder(String connectionString)
        {
            conn.ConnectionString = connectionString;
        }

        public void testConnection()
        {
            // Throws exception in case of any problem, and if there is no stored procedure 'spGetPersonalDetails'
            try
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("select [name] from sys.procedures where name = 'spGetPersonalDetails'", conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows) {
                            throw new Exception("Required stored procedure doesn't exist");
                        }
                    }
                }
                conn.Close();
            } catch (Exception ex)
            {
                conn.Close();
                throw (ex);
            }
        }

        public DataTable getDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("spGetPersonalDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                dt.Load(cmd.ExecuteReader());
                conn.Close();
                return dt;
            }
            catch(Exception ex)
            {
                conn.Close();
                throw (ex);
            }            
        }

        public void update(int id, String first_name, String last_name, String email, String address, String mobile) 
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("spUpdatePersonalDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", SqlDbType.Int).Value = id;
                cmd.Parameters.AddWithValue("@first_name", SqlDbType.Text).Value = first_name;
                cmd.Parameters.AddWithValue("@last_name", SqlDbType.Text).Value = last_name;
                cmd.Parameters.AddWithValue("@email", SqlDbType.Text).Value = email;
                cmd.Parameters.AddWithValue("@address", SqlDbType.Text).Value = address;
                cmd.Parameters.AddWithValue("@mobile", SqlDbType.Text).Value = mobile;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw (ex);
            }
        }

        public void insert(String first_name, String last_name, String email, String address, String mobile)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("spInsertPersonalDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@first_name", SqlDbType.Text).Value = first_name;
                cmd.Parameters.AddWithValue("@last_name", SqlDbType.Text).Value = last_name;
                cmd.Parameters.AddWithValue("@email", SqlDbType.Text).Value = email;
                cmd.Parameters.AddWithValue("@address", SqlDbType.Text).Value = address;
                cmd.Parameters.AddWithValue("@mobile", SqlDbType.Text).Value = mobile;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw (ex);
            }
        }

        public void delete(int id)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("spDeletePersonalDetailsRow", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", SqlDbType.Int).Value = id;                
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw (ex);
            }
        }
    }
}
