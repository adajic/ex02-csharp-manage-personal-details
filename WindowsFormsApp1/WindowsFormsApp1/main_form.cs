﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Data.ConnectionUI;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class main_form : Form
    {
        Connector connector;
        public main_form()
        {            
            InitializeComponent();
            InitializeConnector();
            refreshData();
            selectRowAtIndex(0);
        }

        public void InitializeConnector()
        {
            /*
            SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
            myBuilder.UserID = "sa";
            myBuilder.Password = "1qaz@WSX3edc";
            myBuilder.InitialCatalog = "TestDB";
            myBuilder.DataSource = "DESKTOP-PPTFM8D\\SQLEXPRESS";
            myBuilder.ConnectTimeout = 30;
            String connectionString = myBuilder.ConnectionString;
            */

            /*
            // This is for com.rusanu.dataconnectiondialog
            var str = new SqlConnectionStringBuilder();
            var dialog = new DataConnectionDialog(str);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                connector = new Connector(dialog.ConnectionStringBuilder.ConnectionString);
            }
            */

            String connectionString="";
            String path = Path.GetTempPath()+"/connection_string.txt";
            bool success = false;
            while (success == false)
            {
                if (!File.Exists(path))
                {
                    // Getting connection string from the DataConnectionDialog
                    DataConnectionDialog dcd = new DataConnectionDialog();
                    DataSource.AddStandardDataSources(dcd);
                    dcd.SelectedDataSource = DataSource.SqlDataSource;
                    dcd.SelectedDataProvider = DataProvider.SqlDataProvider;
                    if (DataConnectionDialog.Show(dcd) == DialogResult.OK) {                        
                        try
                        {
                            connectionString = dcd.ConnectionString;
                            connector = new Connector(connectionString);
                            connector.testConnection(); // To test connection
                            // Writing connection string to the file
                            using (StreamWriter writer = new StreamWriter(path))
                            {
                                writer.Write(connectionString);
                                writer.Close();
                                success = true;
                            }
                        }
                        catch (Exception ex) {
                            MessageBox.Show(ex.Message);
                        }
                    } else {
                        MessageBox.Show("Exiting application", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Environment.Exit(1);
                    }                    
                } else {
                    // Reading connection string from the file                    
                    try
                    {
                        using (StreamReader reader = new StreamReader(path))
                        {
                            String line;
                            while ((line = reader.ReadLine()) != null)
                                connectionString = line;
                            reader.Close();
                        }
                        connector = new Connector(connectionString);
                        connector.testConnection(); // To test connection
                        success = true;                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        // Delete the file, because it had wrong connection string
                        try
                        {
                            File.Delete(path);
                        }
                        catch (Exception ex2)
                        {
                            MessageBox.Show(ex2.Message);
                        }
                    }
                } // End of 'File exists' block
            } // End of while (success == false)
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            refreshData();
            selectRowAtIndex(0);
            clearFields();            
        }

        private void clearFields()
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            emailTextBox.Text = "";
            addressTextBox.Text = "";
            mobileTextBox.Text = "";
        }

        public void refreshData()
        {
            try
            {
                dataGridView1.DataSource = connector.getDataTable();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    int id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["id"].Value);
                    String first_name = firstNameTextBox.Text;
                    String last_name = lastNameTextBox.Text;
                    String email = emailTextBox.Text;
                    String address = addressTextBox.Text;
                    String mobile = mobileTextBox.Text;
                    if (first_name.Length == 0 || last_name.Length == 0 || email.Length == 0 || address.Length == 0 || mobile.Length == 0)
                        MessageBox.Show("Please supply all fields. All fields are required...", "Error message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        connector.update(id, first_name, last_name, email, address, mobile);
                        refreshData();
                        selectRowAtIndex(getIndexOfId(id));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private int getIndexOfId(int id)
        {
            for (int i=0; i<dataGridView1.Rows.Count; i++)
            {
                int id_ = Convert.ToInt32(dataGridView1.Rows[i].Cells["id"].Value);
                if (id == id_) return i;                
            }
            return -1;
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            try
            {
                String first_name = firstNameTextBox.Text;
                String last_name = lastNameTextBox.Text;
                String email = emailTextBox.Text;
                String address = addressTextBox.Text;
                String mobile = mobileTextBox.Text;
                if (first_name.Length == 0 || last_name.Length == 0 || email.Length == 0 || address.Length == 0 || mobile.Length == 0)
                    MessageBox.Show("Please supply all fields. All fields are required...", "Error message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    connector.insert(first_name, last_name, email, address, mobile);
                    refreshData();
                    selectRowAtIndex(dataGridView1.Rows.Count-1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    int id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["id"].Value);
                    connector.delete(id);
                    int index = getSelectedIndex();
                    refreshData();
                    selectRowAtIndex(index);
                    replicateSelectedRowIntoFields();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void selectRowAtIndex(int selectedIndex)
        {
            if (selectedIndex < 0) selectedIndex = 0;
            if (selectedIndex >= dataGridView1.Rows.Count)
                selectedIndex = dataGridView1.Rows.Count - 1;
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows[selectedIndex].Selected = true;
                dataGridView1.CurrentCell = dataGridView1.Rows[selectedIndex].Cells[0];
            }
        }

        private int getSelectedIndex()
        {
            if (dataGridView1.SelectedRows.Count > 0)
                return dataGridView1.SelectedRows[0].Index;
            return -1;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            String firstName = firstNameTextBox.Text;
            String lastName = lastNameTextBox.Text;
            String email = emailTextBox.Text;
            String address = addressTextBox.Text;
            String mobile = mobileTextBox.Text;

            refreshData(); // To load all data
            selectRowAtIndex(0);

            for (int i=0; i<dataGridView1.Rows.Count; i++)
            {
                // Now lets remove rows which are not what we are searching for
                if (!dataGridView1.Rows[i].Cells["first_name"].Value.ToString().StartsWith(firstName))
                {       
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["last_name"].Value.ToString().StartsWith(lastName))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["email"].Value.ToString().StartsWith(email))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["address"].Value.ToString().StartsWith(address))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["mobile"].Value.ToString().StartsWith(mobile))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            replicateSelectedRowIntoFields();
        }

        private void replicateSelectedRowIntoFields()
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                firstNameTextBox.Text = dataGridView1.SelectedRows[0].Cells["first_name"].Value.ToString();
                lastNameTextBox.Text = dataGridView1.SelectedRows[0].Cells["last_name"].Value.ToString();
                emailTextBox.Text = dataGridView1.SelectedRows[0].Cells["email"].Value.ToString();
                addressTextBox.Text = dataGridView1.SelectedRows[0].Cells["address"].Value.ToString();
                mobileTextBox.Text = dataGridView1.SelectedRows[0].Cells["mobile"].Value.ToString();
            }
        }
    }
}
