using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualStudio.Data;

namespace WindowsFormsApp1
{
    public partial class main_form : Form
    {
        Connector connector;
        public main_form()
        {            
            InitializeComponent();
            InitializeConnector();
            refreshData(0);
        }

        public void InitializeConnector()
        {
            /*
            SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
            myBuilder.UserID = "sa";
            myBuilder.Password = "1qaz@WSX3edc";
            myBuilder.InitialCatalog = "TestDB";
            myBuilder.DataSource = "DESKTOP-PPTFM8D\\SQLEXPRESS";
            myBuilder.ConnectTimeout = 30;*/
            String connectionString;
            DataConnectionDialog dcd = new DataConnectionDialog();
            DataConnectionConfiguration dcs = new DataConnectionConfiguration(null);
            dcs.LoadConfiguration(dcd);

            if (DataConnectionDialog.Show(dcd) == DialogResult.OK)
            {
                connector = new Connector(connectionString);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            emailTextBox.Text = "";
            addressTextBox.Text = "";
            mobileTextBox.Text = "";
        }

        public void refreshData(int selectedIndex)
        {
            try
            {
                dataGridView1.DataSource = connector.getDataTable();

                if (selectedIndex < 0) selectedIndex = 0;
                if (selectedIndex >= dataGridView1.Rows.Count)
                    selectedIndex = dataGridView1.Rows.Count - 1;

                selectRowAtIndex(selectedIndex);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                firstNameTextBox.Text = dataGridView1.SelectedRows[0].Cells["first_name"].Value.ToString();
                lastNameTextBox.Text = dataGridView1.SelectedRows[0].Cells["last_name"].Value.ToString();
                emailTextBox.Text = dataGridView1.SelectedRows[0].Cells["email"].Value.ToString();
                addressTextBox.Text = dataGridView1.SelectedRows[0].Cells["address"].Value.ToString();
                mobileTextBox.Text = dataGridView1.SelectedRows[0].Cells["mobile"].Value.ToString();
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    int id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["id"].Value);
                    String first_name = firstNameTextBox.Text;
                    String last_name = lastNameTextBox.Text;
                    String email = emailTextBox.Text;
                    String address = addressTextBox.Text;
                    String mobile = mobileTextBox.Text;
                    connector.update(id, first_name, last_name, email, address, mobile);
                    refreshData(getSelectedIndex());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            try
            {
                String first_name = firstNameTextBox.Text;
                String last_name = lastNameTextBox.Text;
                String email = emailTextBox.Text;
                String address = addressTextBox.Text;
                String mobile = mobileTextBox.Text;
                connector.insert(first_name, last_name, email, address, mobile);
                refreshData(dataGridView1.Rows.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    int id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["id"].Value);
                    connector.delete(id);
                    refreshData(getSelectedIndex());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            refreshData(0);
        }

        private void selectRowAtIndex(int selectedIndex)
        {
            if (dataGridView1.Rows.Count > 0 && selectedIndex >= 0 && selectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.Rows[selectedIndex].Selected = true;
                dataGridView1.CurrentCell = dataGridView1.Rows[selectedIndex].Cells[0];
            }
        }

        private int getSelectedIndex()
        {
            if (dataGridView1.SelectedRows.Count > 0)
                return dataGridView1.SelectedRows[0].Index;
            return -1;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            String firstName = firstNameTextBox.Text;
            String lastName = lastNameTextBox.Text;
            String email = emailTextBox.Text;
            String address = addressTextBox.Text;
            String mobile = mobileTextBox.Text;

            refreshData(0); // To load all data

            for (int i=0; i<dataGridView1.Rows.Count; i++)
            {
                // Now lets remove rows which are not what we are searching for
                if (!dataGridView1.Rows[i].Cells["first_name"].Value.ToString().StartsWith(firstName))
                {       
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["last_name"].Value.ToString().StartsWith(lastName))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["email"].Value.ToString().StartsWith(email))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["address"].Value.ToString().StartsWith(address))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
                if (!dataGridView1.Rows[i].Cells["mobile"].Value.ToString().StartsWith(mobile))
                {
                    dataGridView1.Rows.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            
            firstNameTextBox.Text = firstName;
            lastNameTextBox.Text = lastName;
            emailTextBox.Text = email;
            addressTextBox.Text = address;
            mobileTextBox.Text = mobile;
        }
    }
}
